import torch
import cv2

import util

if __name__ == '__main__':
	
	points3D = torch.Tensor(cv2.imread('data/coords0032.exr', cv2.IMREAD_UNCHANGED))
	cam_pose = torch.Tensor(cv2.imread('data/pose0032.exr', cv2.IMREAD_UNCHANGED))

	intrinsic, extrinsic, projection_matrix = util.get_camera_matrices(cam_pose,points3D)
	print('intrisic camera matrix:')
	print(intrinsic)
	print('extrinsic camera matrix:')
	print(extrinsic)
	print('complete 3x4 camera projection matrix')
	print(projection_matrix)