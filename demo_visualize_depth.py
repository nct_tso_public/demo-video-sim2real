import cv2
from matplotlib import cm
import matplotlib.pyplot as plt

import util

if __name__ == '__main__':

	# depth map
	# shape: H x W
	depth = cv2.imread('data/depth0032.exr', cv2.IMREAD_UNCHANGED)
	# normalize
	depth = (depth - depth.min()) / (depth.max() - depth.min())
	# invert depth such that "far = dark" and "near = bright".
	depth = 1 - depth
	# convert to heattmap
	depth = cm.get_cmap('inferno')(depth)[:,:,:3]
	plt.imsave('depth.png',depth)
	print('Saved depth as heatmap in "depth.png".')