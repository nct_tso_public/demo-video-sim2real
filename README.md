### DEMO: Using the Surgical Sim2Real Dataset

This repository contains demo code for using the synthetic dataset which was generated from our ICCV '21 [paper](https://openaccess.thecvf.com/content/ICCV2021/html/Rivoir_Long-Term_Temporally_Consistent_Unpaired_Video_Translation_From_Simulated_Surgical_3D_ICCV_2021_paper.html) for unpaired synthesis of view-consistent surgical video sequences. The dataset can be downloaded from our [project page](http://opencas.dkfz.de/video-sim2real/).

The dataset contains 21 000 random views as well as 11 short video sequences of synthetic, but realistic surgical scenes. We provide the following ground truth information:
- Semantic Segmentation
- Depth
- Pixel-wise 3D Locations
- Camera Poses

Using the pixel-wise 3D coordinates and camera poses, one can e.g. warp pixels from one view to another (see figure below), compute optical flow, find point correspendes, etc.

![example image](figures/example_warp.png)

### Demo Scripts

The demo scripts provide sample code for the following use cases:
- ```demo_load_ground_truth.py```: Loads data to PyTorch tensors and explains their shapes.
- ```demo_visualize_depth.py```: Visualizes a depth map as a heatmap.
- ```demo_camera_matrices.py```: Computes camera matrices from a given camera pose and its corresponding pixel-wise 3D coordinates.
- ```demo_warp.py```: Warps pixels from one view into another (as seen in the figure above). The warping function is fully **differentiable**!
- ```demo_optical_flow.py```: Computes ground-truth optical flow from camera poses and pixel-wise 3D coordinates of 2 consecutive frames.

### Dependences

> torch, torchvision, mathutils, PIL, cv2, matplotlib, numpy

### Citation

This work was presented at the [IEEE/CVF International Conference on Computer Vision 2021](https://iccv2021.thecvf.com/home). If you use this data/code, please cite our paper:

```
@InProceedings{Rivoir_2021_ICCV,
    author    = {Rivoir, Dominik and Pfeiffer, Micha and Docea, Reuben and Kolbinger, Fiona and Riediger, Carina and Weitz, J\"urgen and Speidel, Stefanie},
    title     = {Long-Term Temporally Consistent Unpaired Video Translation From Simulated Surgical 3D Data},
    booktitle = {Proceedings of the IEEE/CVF International Conference on Computer Vision (ICCV)},
    month     = {October},
    year      = {2021},
    pages     = {3343-3353}
}
```

This work was carried out at the National Center for Tumor Diseases (NCT) Dresden, [Department of Translational Surgical Oncology](https://www.nct-dresden.de/tso.html) and the Centre for Tactile Internet ([CeTI](https://ceti.one/)) at TU Dresden.

### Acknowledgements

Funded by the German Research Foundation (DFG, Deutsche Forschungsgemeinschaft) as part of Germany’s Excellence Strategy – EXC 2050/1 – Project ID 390696704 – Cluster of Excellence “Centre for Tactile Internet with Human-in-the-Loop” (CeTI) of Technische Universität Dresden.

### Contact

If you have any questions, do not hesitate to contact us: ```dominik.rivoir [at] nct-dresden.de```